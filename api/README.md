### Create project level of application

from fastapi import FastAPI

app = FastAPI()


### Create folders for Queries that will hold schema for information sent between API endpoints


### Create folder for Routers that will hold equivalent of Django api_views and api_urls called Routers


### start with routers and come up with all endpoint methods similar to wireframe and put pass on them to reference when creating schemas

    * Put just the router with CRUD method and url path as decorator, then a blank view function with a 'pass', so you know what you're creating for the schema.
    * We'll come back to this page. Just creating skeleton for now

### Go to Queries to plan out schema, so we know what kind of migration tables to create
