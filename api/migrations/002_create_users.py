steps = [
    [
        ## Create Table Users
        """
        CREATE TABLE users(
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR(20) NOT NULL,
            last_name VARCHAR(20) NOT NULL,
            email VARCHAR(20) NOT NULL,
            username VARCHAR(20) NOT NULL,
            password VARCHAR(20) NOT NULL,
        );
        """,
        ## Drop Table
        """
        DROP TABLE users;
        """
    ]
]
