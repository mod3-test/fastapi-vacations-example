from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool
from queries.vacations import VacationOut


class UsersIn(BaseModel):
    first_name: str
    last_name: str
    email: str
    username: str
    password: str


class UsersOut(BaseModel):
    user_id: str
    first_name: str
    last_name: str
    email: str
    username: str
    password: str
    is_driver: Optional[bool]




# Below used for encoding password has hash string
# class UsersOutWithPassword(UsersOut):
#     hashed_password: str




class UsersRepository:

    def create_user(self, user: UsersIn) -> UsersOut:
        pass

# TImi updated VactionOut, imported from queries in line 4
    def get_all_users(self) -> List[VacationOut]:
        pass
