from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool
from datetime import date


class Error(BaseModel):
    message: str


class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VacationOut(BaseModel):
    vacation_id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]
