from fastapi import APIRouter



router = APIRouter()



@router.post('/users')
def create_user():
    pass


@router.get('/users')
def get_all_users():
    pass


@router.get('/users/{user_id}')
def get_user():
    pass


@router.update('/users/{user_id}')
def update_user():
    pass


@router.delete('/users/{user_id}')
def delete_user():
    pass
