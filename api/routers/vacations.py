from fastapi import APIRouter, Depends


router = APIRouter()



@router.post('/vacations')
def create_vacation():
    pass


@router.get('/vacations')
def get_all_vacations():
    pass


@router.get('/vacations/{vacation_id}')
def get_vacation():
    pass


@router.put('/vacations/{vacation_id}')
def update_vacation():
    pass


@router.delete('/vacations/{vacation_id}')
def delete_vacation():
    pass
